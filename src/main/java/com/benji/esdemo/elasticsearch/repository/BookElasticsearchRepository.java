package com.benji.esdemo.elasticsearch.repository;

import com.benji.esdemo.book.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookElasticsearchRepository extends ElasticsearchRepository<Book, String> {
}
