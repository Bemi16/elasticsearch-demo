package com.benji.esdemo.elasticsearch.service;

import com.benji.esdemo.book.Book;
import com.benji.esdemo.elasticsearch.repository.BookElasticsearchRepository;
import org.elasticsearch.action.index.IndexAction;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.ElasticsearchClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BookElasticsearchService {

    private final Logger logger = LoggerFactory.getLogger(BookElasticsearchService.class);

    @Autowired
    private BookElasticsearchRepository repository;

    @Autowired
    private ElasticsearchOperations operations;

    public void saveBook(Book book) throws IOException {

        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(book.getId())
                .withObject(book)
                .build();

        IndexCoordinates coord = operations.getIndexCoordinatesFor(Book.class);
        operations.index(indexQuery, coord);

    }

    public List<Book> getBooks() {
        Iterable<Book> books = repository.findAll();
        List<Book> list = new ArrayList<>();
        for(Book b : books) {
            list.add(b);
        }
        return list;
    }

    public void deleteBookById(String id) {
        repository.deleteById(id);
    }
}
