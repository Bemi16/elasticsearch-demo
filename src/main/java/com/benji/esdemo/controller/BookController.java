package com.benji.esdemo.controller;

import com.benji.esdemo.book.Book;
import com.benji.esdemo.elasticsearch.service.BookElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookElasticsearchService service;

    @PostMapping
    public String saveBook(@RequestBody Book book) throws IOException {
        service.saveBook(book);
        return book.toString();
    }

    @GetMapping
    public List<Book> getBooks() {
        return service.getBooks();
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable String id) {
        service.deleteBookById(id);
        return "Book with id: " + id + " deleted successfully";
    }
}
